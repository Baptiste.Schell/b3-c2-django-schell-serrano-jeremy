from django.db import models
from django.contrib.auth.models import User 

class FlightSchool(models.Model):
    user_id = models.ForeignKey(User,on_delete=models.CASCADE)
    name = models.CharField(max_length=40)
    description = models.CharField(max_length=100)
    
    def __str__(self):
        return self.name
    
class FlightSchoolReservation(models.Model):
  user_id =  models.ForeignKey(User,on_delete=models.CASCADE)
  flight_school_id =  models.ForeignKey(FlightSchool,on_delete=models.CASCADE)
  
  reservation_debut = models.DateTimeField(null=True)
  reservation_fin = models.DateTimeField(null=True)

  def __str__(self):
      return str(self.user_id)+" "+ str(self.reservation_debut.strftime("%Y-%m-%d")) +" to "+ str(self.reservation_fin.strftime("%Y-%m-%d"))
