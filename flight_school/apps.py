from django.apps import AppConfig


class FlightSchoolConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'flight_school'

