from datetime import datetime
from django.shortcuts import redirect, render,get_object_or_404
from .models import FlightSchool, FlightSchoolReservation
from .forms import FlightSchoolReservationForm
from django.contrib.auth.models import User 


def getAllFlightSchool(request):
    fs = FlightSchool.objects.all()
    fsr = FlightSchoolReservation.objects.all()
    fsr_current_u = FlightSchoolReservation.objects.filter(user_id=request.user)
    i = 0
    data = {}
    
    for flight_school in fs:
        if len(fsr_current_u) == 0 | len(fsr) == 0:
            data[f"fs{i}"] = {
                'flight_school': flight_school,
                'is_reservation_made': False
            }
            i = i + 1
            continue
        
        fs_current_r = FlightSchoolReservation.objects.filter(flight_school_id=flight_school,user_id=request.user)
        
        if len(fs_current_r) == 0:
            data[f"fs{i}"] = {
                'flight_school': flight_school,
                'is_reservation_made': False
            }
        elif len(fs_current_r) > 0:
            data[f"fs{i}"] = {
                'flight_school': flight_school,
                'flight_school_reservation':fs_current_r,
                'is_reservation_made': True
            }   
        
        i = i + 1
    
    
    context = {'data':data,}
    
    return render(request,'list_flight_schools.html',context)



def makeReservation(request, id:int):
    flight_school = get_object_or_404(FlightSchool,pk=id)
    reservation_form = FlightSchoolReservationForm()
    context = {'flight_school':flight_school,'reservation_form':reservation_form,'page_mode':'make'}
   
    
    if request.method == 'POST':
        reservation_form = FlightSchoolReservationForm(request.POST)
        reservation_debut = reservation_form.data['reservation_debut']
        reservation_fin = reservation_form.data['reservation_fin']
        reservation_debut_date = datetime.strptime(reservation_debut, "%Y-%m-%d")
        reservation_fin_date = datetime.strptime(reservation_fin, "%Y-%m-%d")
        
        if reservation_fin_date < reservation_debut_date:
            error = 'La date de fin doit être superieur à la date de début'
        elif reservation_fin_date == reservation_debut_date:
            error = 'La date de début et de fin doivent être différentes'
        elif reservation_fin_date > reservation_debut_date:
            error = ''
            
        context['errors'] = error
        
        if reservation_fin_date > reservation_debut_date:
            if reservation_form.is_valid():
                form_submit = reservation_form.save(commit=False)
                form_submit.user_id =  request.user
                form_submit.flight_school_id = flight_school
                form_submit.save()  
                return redirect('/')
        
    return render(request,'detail_flight_school.html',context)


def updateReservation(request,id:int):
    
    flight_school_reservation = FlightSchoolReservation.objects.get(id=id)
    reservation_form = FlightSchoolReservationForm(instance=flight_school_reservation)
    confirm_msg = ''
    error =''
  
  
    if request.method == 'POST':
        reservation_form = FlightSchoolReservationForm(request.POST,instance=flight_school_reservation)
        reservation_debut = reservation_form.data['reservation_debut']
        reservation_fin = reservation_form.data['reservation_fin']
        reservation_debut_date = datetime.strptime(reservation_debut, "%Y-%m-%d")
        reservation_fin_date = datetime.strptime(reservation_fin, "%Y-%m-%d")
        
        if reservation_fin_date < reservation_debut_date:
            error = 'La date de fin doit être superieur à la date de début'
        elif reservation_fin_date == reservation_debut_date:
            error = 'La date de début et de fin doivent être différentes'
        
        if reservation_fin_date > reservation_debut_date:
            if reservation_form.is_valid():
                reservation_form.save()  
                confirm_msg = 'Réservation effectué'
                return redirect('/')

     
    context = {'page_mode':'update','reservation_form':reservation_form,'errors':error,'confirm_msg':confirm_msg}
        
    return render(request,'detail_flight_school.html',context)
    
    
    
def doUserMadeReservationAlready(auth_user:User,flight_school_reservation:FlightSchoolReservation):

    if flight_school_reservation.user_id == auth_user:
        return True
    
    return False

def deleteReservation(request,id:int):
    flight_school_reservation = FlightSchoolReservation.objects.get(id=id)
    context = {'flight_school_reservation':flight_school_reservation}
    
    if request.method == 'POST':
        flight_school_reservation.delete()
        return redirect('/')
        
    return render(request, 'delete_flight_school_from_reservation.html',context)

def allReservationsForFlightSchoolId(request):
    flight_school = FlightSchool.objects.get(user_id=request.user)
    flight_school_reservation = FlightSchoolReservation.objects.filter(flight_school_id=flight_school)
    
    
    context = {'flight_school_reservation':flight_school_reservation,'flight_school':flight_school}
        
    return render(request, 'list_reservations.html',context)
