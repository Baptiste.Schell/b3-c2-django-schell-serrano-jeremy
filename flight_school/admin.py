from django.contrib import admin
from .models import FlightSchool,FlightSchoolReservation

admin.site.register(FlightSchool)
admin.site.register(FlightSchoolReservation)
