from django.urls import path
from .views import getAllFlightSchool,makeReservation,updateReservation,deleteReservation, allReservationsForFlightSchoolId

urlpatterns = [
    path('list/', getAllFlightSchool, name="list_schools"),
    path('<int:id>-make-reservation', makeReservation,name="make-reservation"),
    path('<int:id>-update-reservation', updateReservation,name="update-reservation"),
    path('<int:id>-delete-reservation', deleteReservation,name="delete-reservation"),
    path('all-reservations-flight_school', allReservationsForFlightSchoolId,name="all-reservations-flight_school"),
]