from django import forms
from django.forms import ModelForm
from .models import FlightSchoolReservation

class FlightSchoolReservationForm(ModelForm):
    class Meta:
        model = FlightSchoolReservation
        exclude = ['user_id','flight_school_id']   
        fields = ['reservation_debut','reservation_fin','user_id','flight_school_id']
        widgets = {
            'reservation_debut': forms.DateInput(format="%Y-%m-%d"),
            'reservation_fin': forms.DateInput(format="%Y-%m-%d"),
            'user_id': forms.HiddenInput(),
            'flight_school_id': forms.HiddenInput(),         
        }