# Django - Ecole de Pilotage

## Sommaire
[Dépendances](#dépendances) 
**|** [Installation et exécution](#installation-et-exécution)
**|** [Commandes utiles](#commandes-utiles)
**|** [Users de test](#users-de-testt)

___
## Dépendances

- [Python](https://www.python.org/downloads/)   
- [Django](https://www.djangoproject.com/download/)

___
## Installation et exécution


Installer Django version 4.1.7

```
pip install django
```

Installer Boostrap version 5
```
pip install django-bootstrap-v5
```

Lancer le projet
```
python manage.py runserver
```

___
## Commandes utiles 

- python manage.py migrate --run-syncdb 

- python manage.py makemigrations

- python manage.py createsuperuser

___
## Users de test

Utilisateur admin : 
>Login : admin  
>Password : admin
___
Utilisateur user : 
>Login : baptiste  
>Password : aqzsedrf
___
Utilisateur école : 
>Login : Ecole_pilotage_Paris  
>Password : aqzsedrftg

Utilisateur école : 
>Login : Ecole_pilotage_Toulouse  
>Password : aqzsedrftg

